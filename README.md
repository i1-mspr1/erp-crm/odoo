# Odoo

## Installation

Create a docker compose file like this:

```
version: '1.0'
services:
  web:
    image: odoo:15.0
    depends_on:
      - db
    ports:
      - "8069:8069"
    volumes:
      - odoo-web-data:/var/lib/odoo
        # - ./config:/etc/odoo
      - ./addons:/mnt/extra-addons
    environment:
      - PASSWORD_FILE=/run/secrets/postgresql_password
      - USER=${user_name}
    secrets:
      - postgresql_password
  db:
    image: postgres:13
    ports:
      - "5432:5432"
    environment:
      - POSTGRES_DB=${db_name}
      - POSTGRES_PASSWORD_FILE=/run/secrets/postgresql_password
      - POSTGRES_USER=${user_name}
      - PGDATA=/var/lib/${path}
    volumes:
      - odoo-db-data:/var/lib/${path}
    secrets:
      - postgresql_password
volumes:
  odoo-web-data:
  odoo-db-data:

secrets:
  postgresql_password:
    file: odoo_pg_pass
```

Don't forget to create a file with a password in it at the root of the docker compose like  `odoo_pg_pass`.
